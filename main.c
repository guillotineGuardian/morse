#include <stdio.h>
#include <string.h>

#ifndef BUFSIZE
#define BUFSIZE 4096
#else
printf("Error: BUFSIZE already defined.\n\n")
exit(17);
#endif

int main ()
{
	char buffer[BUFSIZE] = {'\0'};
	int i;
	printf("NOTE: Input must be capitalized. Punctuation is not "
							"recognized.\n\n");
	printf("Enter string (no more than %d characters long)"
							": ", BUFSIZE - 1);
	fgets(buffer, BUFSIZE - 1, stdin);
	for (i = 0; i < strlen(buffer) - 1; i++) {
		convert(buffer[i]);
	}
	printf("\n\n");
	return 0;
}
